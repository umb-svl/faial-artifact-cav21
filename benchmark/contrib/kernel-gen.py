#!/usr/bin/env python3
import jinja2
import argparse
import sys
import json

from pathlib import Path

def convert_scalar(elems):
    result = []
    for elem in elems:
        # a: int
        key, = elem.keys()
        contents = elem[key]
        result.append({"name": key, "type": contents})
    return result


def convert_array(elems, default_size):
    result = []
    for elem in elems:
        # a: int or a: {type: int, size: 10}
        key, = elem.keys()
        contents = elem[key]
        if isinstance(contents, str):
            contents = {"type": contents, "size": default_size}

        contents["name"] = key
        result.append(contents)
    return result

def load_tpl():
    tpl = """{% extends "main.base" %}
{%- block header %}
{{ header }}
{%- endblock -%}
{%- block body %}
{{ body }}
{%- endblock -%}
"""
    search_path = Path(__file__).parent / "tpl"
    fs = jinja2.FileSystemLoader(searchpath=str(search_path))
    return jinja2.Environment(loader=fs).from_string(tpl)

def load_file(filename):
    if filename.suffix == ".json":
        import json
        loader = json.load
    elif filename.suffix == ".toml":
        import tomlkit
        loader = lambda x: tomlkit.loads(x.read())
    else:
        import yaml
        loader = yaml.safe_load
    return loader(filename.open())

def generate(template, data):
    yaml_data = load_file(template)
    data["body"] = yaml_data["body"]
    data["header"] = yaml_data.get("header", "")
    data["arrays"] = convert_array(
        yaml_data.get("arrays", ()),
        data["array_size"]
    )
    data["scalars"] = convert_scalar(yaml_data.get("scalars", ()))
    data["includes"] = yaml_data.get("includes", [])
    data["pre"] = yaml_data.get("pre", [])
    data["grid_dim"] = yaml_data.get("grid_dim", [])
    if isinstance(data["grid_dim"], int):
        data["grid_dim"] = [ data["grid_dim"] ]
    data["block_dim"] = yaml_data.get("block_dim", [])
    if isinstance(data["block_dim"], int):
        data["block_dim"] = [ data["block_dim"] ]
    return load_tpl().render(**data)

def main():
    parser = argparse.ArgumentParser(description='Generate kernel compatible with multiple verification tools.')
    parser.add_argument('-o', dest='output', help="Where to store output file. Default: standard output")
    parser.add_argument('--array-size', type=int, default=1024, help="Symbolic-exec tools require a known array size. Default: %(default)s")
    parser.add_argument('--tool', '-t', choices=["gpuverify", "esbmc", "pug", "gklee"], default="gpuverify", help="Code-generation compatible with the given tool. Default: %(default)s")
    parser.add_argument('filename')
    args = parser.parse_args()
    out = sys.stdout if args.output is None else open(args.output, "w")
    try:
        out.write(generate(Path(args.filename), vars(args)))
        out.write("\n")
    finally:
        if args.output is not None:
            out.close()

if __name__ == '__main__':
    import sys
    main()
