#!/usr/bin/env python3
import argparse
import sys

def get_error(fp):
    lines = fp.readlines()
    for line in lines:
        if "Segmentation fault      (core dumped)" in line:
            return 'error:segfault'
        if ".bc': No such file or directory" in line:
            return 'error:nvcc'
        if "memory error: out of bound pointer" in line:
            return 'error:array_size'
        if "write-read race" in line:
            return 'racy'
        if "read-write race" in line:
            return 'racy'
        if "write-write race" in line:
            return 'racy'
        if "********** Race found **********" in line:
            return 'racy'
    return 'no race alarms'

def parse(status, log):
    with open(log) as fp:
        err = get_error(fp)
    if status is None:
        return err
    if err == 'error:array_size':
        return err
    if status == 124:
        return 'timeout'
    if err in 'racy':
        # gklee can timeout but still return racy
        assert status in [0, 124], status
        return err
    if status == 139:
        # Question: What's causing gklee's segfaults?
        assert err == 'error:segfault', err
        return err
    if status == 1:
        assert err.startswith('error'), err
        return err
    if err == 'no race alarms':
        assert status == 0, status
        return err
    print("ERROR: unknown exit status", status, log, file=sys.stderr)
    return "unknown"

def main():
    parser = argparse.ArgumentParser(description="Parse a gklee log.")
    parser.add_argument('--status', type=int, help="The exit status.")
    parser.add_argument('log', help="The output of gklee.")
    args = parser.parse_args()
    print(parse(args.status, args.log))

if __name__ == '__main__':
    main()
