#!/bin/bash
# Converts the filename from '.cu' to '.toml'
get_toml() {
    cut -d, -f4 | sed 's/faial\///' | sed 's/.cu$/.toml/'
}
grep ",drf" timings-faial.csv | get_toml > ok.txt &&
grep ",racy" timings-faial.csv | get_toml > fail.txt &&
grep ",error" timings-faial.csv | get_toml > err.txt &&
grep ",timeout" timings-faial.csv | get_toml > timeout.txt
