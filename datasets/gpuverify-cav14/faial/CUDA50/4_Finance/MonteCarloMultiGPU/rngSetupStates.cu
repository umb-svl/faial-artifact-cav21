//pass
//--blockDim=[256] --gridDim=[256]

#include <cuda.h>






__global__ void kernel (curandState* rngState, unsigned long long seed,unsigned long long offset) {



    // determine global thread id
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    // Each thread gets the same seed, a different
    // sequence number. A different offset is used for
    // each device.
    curand_init(seed, tid, offset, &rngState[tid]);

}
