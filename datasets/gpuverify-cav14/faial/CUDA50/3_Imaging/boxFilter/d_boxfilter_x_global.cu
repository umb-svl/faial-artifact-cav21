//pass
//--blockDim=[64] --gridDim=[16]

#include <cuda.h>






__global__ void kernel (float* id, float* od, int w,int h,int r) {

/* kernel pre-conditions */

__requires(w == 1024);

__requires(h == 1024);

__requires(r ==   14);





    unsigned int y = blockIdx.x*blockDim.x + threadIdx.x;
    float scale = 1.0f / (float)((r << 1) + 1);

    float t;
    // do left edge
    t = id[y * w] * r;

    for (int x = 0; x < (r + 1); x++)
    {
        t += id[y * w + x];
    }

    od[y * w] = t * scale;

    for (int x = 1;
         x < (r + 1); x++)
    {
        t += id[y * w + x + r];
        t -= id[y * w];
        od[y * w + x] = t * scale;
    }

    // main loop
    for (int x = (r + 1);
         x < w - r; x++)
    {
        t += id[y * w + x + r];
        t -= id[y * w + x - r - 1];
        od[y * w + x] = t * scale;
    }

    // do right edge
    for (int x = w - r;
         x < w; x++)
    {
        t += id[y * w + w - 1];
        t -= id[y * w + x - r - 1];
        od[y * w + x] = t * scale;
    }

}
