//pass
//--blockDim=[256,1,1] --gridDim=[1200,1,1]

#include <cuda.h>






__global__ void kernel (const float* op1, const float* op2, float* sum, int count) {



    const int pos = threadIdx.x + blockIdx.x * blockDim.x;

    if (pos >= count) return;

    sum[pos] = op1[pos] + op2[pos];

}
