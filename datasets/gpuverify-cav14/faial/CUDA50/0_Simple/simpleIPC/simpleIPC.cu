//pass
//--blockDim=[512] --gridDim=[8]

#include <cuda.h>






__global__ void kernel (int* dst, int* src, int num) {



    // Dummy kernel
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    dst[idx] = src[idx] / num;

}
