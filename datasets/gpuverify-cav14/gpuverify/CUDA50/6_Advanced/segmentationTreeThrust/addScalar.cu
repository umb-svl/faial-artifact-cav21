//pass
//--blockDim=[256,1,1] --gridDim=[11377,1,1]

#include <cuda.h>
#include "common.h"






__global__ void kernel (uint* array, int scalar,uint size) {



    uint tid = blockIdx.x * blockDim.x + threadIdx.x;

    if (tid < size)
    {
        array[tid] += scalar;
    }

}
