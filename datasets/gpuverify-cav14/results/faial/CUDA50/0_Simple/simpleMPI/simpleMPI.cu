//pass
//--blockDim=[256] --gridDim=[10000]

#include <cuda.h>






__global__ void kernel (float* input, float* output) {



    int tid = blockIdx.x * blockDim.x + threadIdx.x;
    output[tid] = sqrt(input[tid]);

}
