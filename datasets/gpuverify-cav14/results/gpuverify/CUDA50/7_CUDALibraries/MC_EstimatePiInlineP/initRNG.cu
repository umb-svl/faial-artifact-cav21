//pass
//--blockDim=[128] --gridDim=[195]

#include <cuda.h>






__global__ void kernel (curandState* const rngStates, const unsigned int seed) {



    // Determine thread ID
    unsigned int tid = blockIdx.x * blockDim.x + threadIdx.x;

    // Initialise the RNG
    curand_init(seed, tid, 0, &rngStates[tid]);

}
