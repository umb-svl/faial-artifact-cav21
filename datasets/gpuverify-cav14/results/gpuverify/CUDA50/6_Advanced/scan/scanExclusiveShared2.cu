//pass
//--blockDim=[256] --gridDim=[26]

#include <cuda.h>

#define THREADBLOCK_SIZE 256





__global__ void kernel (uint* d_Buf, uint* d_Dst, uint* d_Src, uint N,uint arrayLength) {

/* kernel pre-conditions */

__requires(N == 6656);

__requires((arrayLength & (arrayLength - 1)) == 0);




    __shared__ uint s_Data[2 * THREADBLOCK_SIZE];

    //Skip loads and stores for inactive threads of last threadblock (pos >= N)
    uint pos = blockIdx.x * blockDim.x + threadIdx.x;

    //Load top elements
    //Convert results of bottom-level scan back to inclusive
    uint idata = 0;

    if (pos < N)
        idata =
            d_Dst[(4 * THREADBLOCK_SIZE) - 1 + (4 * THREADBLOCK_SIZE) * pos] +
            d_Src[(4 * THREADBLOCK_SIZE) - 1 + (4 * THREADBLOCK_SIZE) * pos];

    //Compute
    uint x_pos = 2 * threadIdx.x - (threadIdx.x & (arrayLength - 1));
    s_Data[x_pos] = 0;
    x_pos += arrayLength;
    s_Data[x_pos] = idata;

    for (uint offset = 1; offset < arrayLength; offset <<= 1)
    {
        __syncthreads();
        uint t = s_Data[x_pos] + s_Data[x_pos - offset];
        __syncthreads();
        s_Data[x_pos] = t;
    }

    uint odata = s_Data[x_pos] - idata;

    //Avoid out-of-bound access
    if (pos < N)
    {
        d_Buf[pos] = odata;
    }

}
