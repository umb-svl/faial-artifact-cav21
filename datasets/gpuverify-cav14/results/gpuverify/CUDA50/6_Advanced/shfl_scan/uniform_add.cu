//pass
//--blockDim=[256] --gridDim=[255]

#include <cuda.h>






__global__ void kernel (int* data, int* partial_sums, int len) {

/* kernel pre-conditions */

__requires(len == 65536);





    __shared__ int buf;
    int id = ((blockIdx.x * blockDim.x) + threadIdx.x);

    if (id > len) return;

    if (threadIdx.x == 0)
    {
        buf = partial_sums[blockIdx.x];
    }

    __syncthreads();
    data[id] += buf;

}
