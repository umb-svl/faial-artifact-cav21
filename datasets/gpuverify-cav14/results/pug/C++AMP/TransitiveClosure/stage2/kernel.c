#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

#define _2D_ACCESS(A, y, x, X_DIM) A[(y)*(X_DIM)+(x)]
//////////////////////////////////////////////////////////////////////////////
//// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
//// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
//// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
//// PARTICULAR PURPOSE.
////
//// Copyright (c) Microsoft Corporation. All rights reserved
//////////////////////////////////////////////////////////////////////////////
//----------------------------------------------------------------------------
// File: TransitiveClosure.cpp
//
// Contains the implementation of algorithms which explores connectivity between 
// nodes in a graph and determine shortest path.
// This is based on paper http://www.seas.upenn.edu/~kiderj/research/papers/APSP-gh08-fin-T.pdf
//----------------------------------------------------------------------------
// Defines to help with AMP->OpenCL translation
#define X_DIMENSION 0
#define Y_DIMENSION 1
// Constants - specifies tile size
#define TILE_SIZE (1 << 3)
// State of connection
#define UNCONNECTED 0
#define DIRECTLY_CONNECTED 1
#define INDIRECTLY_CONNECTED 2
#define num_vertices (1 << 6)
//----------------------------------------------------------------------------
// Stage2 - determine connectivity between vertexs' between 2 TILE - primary 
// and current - current is along row or column of primary
//----------------------------------------------------------------------------





__global__ void kernel (unsigned int* graph, int passnum) {




__requires(blockDim.x == 8);


__requires(blockDim.y == 8);




__requires(gridDim.x == 1);


__requires(gridDim.y == 1);




  // Load primary block into shared memory (primary_block_buffer)
  __shared__ unsigned int primary_block_buffer[TILE_SIZE][TILE_SIZE];
  int idxY = passnum * TILE_SIZE + threadIdx.y;
  int idxX = passnum * TILE_SIZE + threadIdx.x;

  primary_block_buffer[threadIdx.y][threadIdx.x] = _2D_ACCESS(graph, idxY, idxX, num_vertices);

  // Load the current block into shared memory (curr_block_buffer)
  __shared__ unsigned int curr_block_buffer[TILE_SIZE][TILE_SIZE];
  unsigned int group_id0
    = blockIdx.y == 0
        ? passnum
        : (blockIdx.x < passnum ? blockIdx.x : blockIdx.x + 1);
  unsigned int group_id1
    = blockIdx.y == 0
        ? (blockIdx.x < passnum ? blockIdx.x : blockIdx.x + 1)
        : passnum;

  idxY = group_id0 * TILE_SIZE + threadIdx.y;
  idxX = group_id1 * TILE_SIZE + threadIdx.x;
  curr_block_buffer[threadIdx.y][threadIdx.x] = _2D_ACCESS(graph, idxY, idxX, num_vertices);

  __syncthreads();

  // Now perform the actual Floyd-Warshall algorithm on this block
  for (unsigned int k = 0;
                k < TILE_SIZE; ++k)
  {
    
    if ( curr_block_buffer[threadIdx.y][threadIdx.x] == UNCONNECTED)
    {
      if (blockIdx.y == 0)
      {
        if ( (primary_block_buffer[threadIdx.y][k] != UNCONNECTED) && (curr_block_buffer[k][threadIdx.x] != UNCONNECTED) )
        {
          curr_block_buffer[threadIdx.y][threadIdx.x] = passnum*TILE_SIZE + k + INDIRECTLY_CONNECTED;
        }
      }
      else
      {
        if ( (curr_block_buffer[threadIdx.y][k] != UNCONNECTED) && (primary_block_buffer[k][threadIdx.x] != UNCONNECTED) )
        {
          curr_block_buffer[threadIdx.y][threadIdx.x] = passnum*TILE_SIZE + k + INDIRECTLY_CONNECTED;
        }
      }
    }

    __syncthreads();
  }

  _2D_ACCESS(graph, idxY, idxX, num_vertices) = curr_block_buffer[threadIdx.y][threadIdx.x];

}
