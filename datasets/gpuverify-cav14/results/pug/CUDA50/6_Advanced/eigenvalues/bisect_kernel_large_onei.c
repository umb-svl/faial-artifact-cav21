#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif
#include "common.h"

////////////////////////////////////////////////////////////////////////////////
//! Determine eigenvalues for large matrices for intervals that after
//! the first step contained one eigenvalue
//! @param  g_d  diagonal elements of symmetric, tridiagonal matrix
//! @param  g_s  superdiagonal elements of symmetric, tridiagonal matrix
//! @param  n    matrix size
//! @param  num_intervals  total number of intervals containing one eigenvalue
//!                         after the first step
//! @param g_left  left interval limits
//! @param g_right  right interval limits
//! @param g_pos  index of interval / number of intervals that are smaller than
//!               right interval limit
//! @param  precision  desired precision of eigenvalues
////////////////////////////////////////////////////////////////////////////////





__global__ void kernel (float* g_d, float* g_s, float* g_left, float* g_right, int* g_pos, int n,int num_intervals,float precision) {




__requires(blockDim.x == 256);





__requires(gridDim.x == 1);





const unsigned int gtid = (blockDim.x * blockIdx.x) + threadIdx.x;

__shared__  float  s_left_scratch[MAX_THREADS_BLOCK];
__shared__  float  s_right_scratch[MAX_THREADS_BLOCK];

// active interval of thread
// left and right limit of current interval
float left, right;
// number of threads smaller than the right limit (also corresponds to the
// global index of the eigenvalues contained in the active interval)
unsigned int right_count;
// flag if current thread converged
unsigned int converged = 0;
// midpoint when current interval is subdivided
float mid = 0.0f;
// number of eigenvalues less than mid
unsigned int mid_count = 0;

// read data from global memory
if (gtid < num_intervals)
{
    left = g_left[gtid];
    right = g_right[gtid];
    right_count = g_pos[gtid];
}


// flag to determine if all threads converged to eigenvalue
__shared__  unsigned int  converged_all_threads;

// initialized shared flag
if (0 == threadIdx.x)
{
    converged_all_threads = 0;
}

__syncthreads();

// process until all threads converged to an eigenvalue
// while( 0 == converged_all_threads)
while (true)
{

    converged_all_threads = 1;

    // update midpoint for all active threads
    if ((gtid < num_intervals) && (0 == converged))
    {

        mid = computeMidpoint(left, right);
    }

    // find number of eigenvalues that are smaller than midpoint
    mid_count = computeNumSmallerEigenvalsLarge(g_d, g_s, n,
                                                mid, gtid, num_intervals,
                                                s_left_scratch,
                                                s_right_scratch,
                                                converged);

    __syncthreads();

    // for all active threads
    if ((gtid < num_intervals) && (0 == converged))
    {

        // udpate intervals -- always one child interval survives
        if (right_count == mid_count)
        {
            right = mid;
        }
        else
        {
            left = mid;
        }

        // check for convergence
        float t0 = right - left;
        float t1 = max(abs(right), abs(left)) * precision;

        if (t0 < min(precision, t1))
        {

            float lambda = computeMidpoint(left, right);
            left = lambda;
            right = lambda;

            converged = 1;
        }
        else
        {
            converged_all_threads = 0;
        }
    }

    __syncthreads();

    if (1 == converged_all_threads)
    {
        break;
    }

    __syncthreads();
}

// write data back to global memory
__syncthreads();

if (gtid < num_intervals)
{
    // intervals converged so left and right interval limit are both identical
    // and identical to the eigenvalue
    g_left[gtid] = left;
}

}
