#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

texture<float, cudaTextureTypeCubemap> tex;





__global__ void kernel (float* g_odata) {




__requires(blockDim.x == 8);


__requires(blockDim.y == 8);


__requires(blockDim.z == 1);



__requires(gridDim.x == 8);


__requires(gridDim.y == 8);


__requires(gridDim.z == 1);



int width = 8*8 /*gridDim.x * blockDim.x*/;

// calculate this thread's data point
unsigned int x = blockIdx.x*blockDim.x + threadIdx.x;
unsigned int y = blockIdx.y*blockDim.y + threadIdx.y;

// 0.5f offset and division are necessary to access the original data points
// in the texture (such that bilinear interpolation will not be activated).
// For details, see also CUDA Programming Guide, Appendix D

float u = ((x+0.5f) / (float) width) * 2.f - 1.f;
float v = ((y+0.5f) / (float) width) * 2.f - 1.f;

float cx, cy, cz;

for (unsigned int face = 0; face < 6; face ++)
{
    //Layer 0 is positive X face
    if (face == 0)
    {
        cx = 1;
        cy = -v;
        cz = -u;
    }
    //Layer 1 is negative X face
    else if (face == 1)
    {
        cx = -1;
        cy = -v;
        cz = u;
    }
    //Layer 2 is positive Y face
    else if (face == 2)
    {
        cx = u;
        cy = 1;
        cz = v;
    }
    //Layer 3 is negative Y face
    else if (face == 3)
    {
        cx = u;
        cy = -1;
        cz = -v;
    }
    //Layer 4 is positive Z face
    else if (face == 4)
    {
        cx = u;
        cy = -v;
        cz = 1;
    }
    //Layer 4 is negative Z face
    else if (face == 5)
    {
        cx = -u;
        cy = -v;
        cz = -1;
    }

    // read from texture, do expected transformation and write to global memory
    g_odata[face*width*width + y*width + x] = -texCubemap(tex, cx, cy, cz);
}
}
