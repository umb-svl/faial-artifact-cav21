#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

//REQUIRES: shfl intrinsic
#define warpSize 32
__device__ int __shfl_up(int, int, int);





__global__ void kernel (unsigned int* img, int width,int height) {

/* kernel pre-conditions */

__requires(width == 1920);





__requires(blockDim.x == 32);


__requires(blockDim.y == 8);




__requires(gridDim.x == 60);


__requires(gridDim.y == 1);





    __shared__ unsigned int sums[32][9];
    int tidx = blockIdx.x * blockDim.x + threadIdx.x;
    //int warp_id = threadIdx.x / warpSize ;
    unsigned int lane_id = tidx % 8;
    //int rows_per_thread = (height / blockDim. y) ;
    //int start_row = rows_per_thread * threadIdx.y;
    unsigned int stepSum = 0;

    sums[threadIdx.x][threadIdx.y] = 0;
    __syncthreads();

    for (int step = 0 ;
         step < 135 ; step++)
    {
        unsigned int sum = 0;

        sum = img[(threadIdx.y+step*8)*width + tidx];
        sums[threadIdx.x][threadIdx.y] = sum;
        __syncthreads();

        // place into SMEM
        // shfl scan reduce the SMEM, reformating so the column
        // sums are computed in a warp
        // then read out properly
        int partial_sum = 0;
        int j = threadIdx.x %8;
        int k = threadIdx.x/8 + threadIdx.y*4;

        partial_sum = sums[k][j];

        for (int i=1 ; i<=8 ; i*=2)
        {
            int n = __shfl_up(partial_sum, i, 32);

            if (lane_id >= i) partial_sum += n;
        }

        sums[k][j] = partial_sum;
        __syncthreads();

        if (threadIdx.y > 0)
        {
            sum += sums[threadIdx.x][threadIdx.y-1];
        }

        sum += stepSum;
        stepSum += sums[threadIdx.x][blockDim.y-1];
        __syncthreads();
        img[(threadIdx.y+step*8)*width + tidx] = sum ;
    }


}
