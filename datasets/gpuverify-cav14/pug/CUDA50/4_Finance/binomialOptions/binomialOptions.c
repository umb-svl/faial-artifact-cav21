#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

//REQUIRES: const array as formal (imperial edit)

#define min(x,y) (x < y ? x : y)

#ifndef DOUBLE_PRECISION
typedef float real;
#else
typedef double real;
#endif
#
//Number of time steps
#define   NUM_STEPS 2048
//Max option batch size
#define MAX_OPTIONS 1024

#define  TIME_STEPS 16
#define CACHE_DELTA (2 * TIME_STEPS)           
#define  CACHE_SIZE (256)
#define  CACHE_STEP (CACHE_SIZE - CACHE_DELTA)

#if NUM_STEPS % CACHE_DELTA
#error Bad constants
#endif

//Preprocessed input option data
typedef struct
{
    real S;
    real X;
    real vDt;
    real puByDf;
    real pdByDf;
} __TOptionData;

#if 0 // imperial edit
static __constant__ __TOptionData d_OptionData[MAX_OPTIONS];
static __device__           float d_CallValue[MAX_OPTIONS];
static __device__            real d_CallBuffer[MAX_OPTIONS * (NUM_STEPS + 16)];
#endif

////////////////////////////////////////////////////////////////////////////////
// Overloaded shortcut functions for different precision modes
////////////////////////////////////////////////////////////////////////////////
#ifndef DOUBLE_PRECISION
__device__ static __attribute__((always_inline)) float expiryCallValue(float S, float X, float vDt, int i)
{
    real d = S * expf(vDt * (2.0f * i - NUM_STEPS)) - X;
    return (d > 0) ? d : 0;
}
#else
__device__ static __attribute__((always_inline)) double expiryCallValue(double S, double X, double vDt, int i)
{
    double d = S * exp(vDt * (2.0 * i - NUM_STEPS)) - X;
    return (d > 0) ? d : 0;
}
#endif

////////////////////////////////////////////////////////////////////////////////
// GPU kernel
////////////////////////////////////////////////////////////////////////////////





__global__ void kernel (__TOptionData* d_OptionData, float* d_CallValue, real* d_CallBuffer) {




__requires(blockDim.x == 256);





__requires(gridDim.x == 512);





__shared__ real callA[CACHE_SIZE+1];
__shared__ real callB[CACHE_SIZE+1];
//Global memory frame for current option (thread block)
// JPW: d_CallBuffer is chopped into contiguous segments, one per block
real *const d_Call = &d_CallBuffer[blockIdx.x * (NUM_STEPS + 16)];

const int       tid = threadIdx.x;
const real      S = d_OptionData[blockIdx.x].S;
const real      X = d_OptionData[blockIdx.x].X;
const real    vDt = d_OptionData[blockIdx.x].vDt;
const real puByDf = d_OptionData[blockIdx.x].puByDf;
const real pdByDf = d_OptionData[blockIdx.x].pdByDf;

//Compute values at expiry date
for (int i = tid; i <= NUM_STEPS; i += CACHE_SIZE)
{
    d_Call[i] = expiryCallValue(S, X, vDt, i);
}

//Walk down binomial tree
//So double-buffer and synchronize to avoid read-after-write hazards.
for (int i = NUM_STEPS; i > 0; i -= CACHE_DELTA)
    for (int c_base = 0; c_base < i; c_base += CACHE_STEP)
    {
        //Start and end positions within shared memory cache
        int c_start = min(CACHE_SIZE - 1, i - c_base);
        int c_end   = c_start - CACHE_DELTA;

        //Read data(with apron) to shared memory
        __syncthreads();

        if (tid <= c_start)
        {
          callA[tid] = d_Call[c_base + tid];
        }

        //Calculations within shared memory
        for (int k = c_start - 1; k >= c_end;)
        {
            //Compute discounted expected value
            __syncthreads();
            callB[tid] = puByDf * callA[tid + 1] + pdByDf * callA[tid];
            k--;

            //Compute discounted expected value
            __syncthreads();
            callA[tid] = puByDf * callB[tid + 1] + pdByDf * callB[tid];
            k--;
        }

        //Flush shared memory cache
        __syncthreads();

        if (tid <= c_end)
        {
            d_Call[c_base + tid] = callA[tid];
        }
    }

//Write the value at the top of the tree to destination buffer
if (threadIdx.x == 0)
{
    d_CallValue[blockIdx.x] = (float)callA[0];
}

}
