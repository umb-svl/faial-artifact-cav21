#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

#define dimX 512
#define dimY 512
//--------------------------------------------------------------------------------------
// File: ocean_simulator.cpp
//
// Main class of ocean simulation
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
#define dmap_dim 512
#define actual_dim dmap_dim
#define input_width (actual_dim + 4)
    // We use full sized data here. The value "output_width" should be actual_dim/2+1 though.
#define output_width actual_dim
#define output_height actual_dim
#define dtx_offset (actual_dim * actual_dim)
#define dty_offset (actual_dim * actual_dim * 2)
// Pre-FFT data preparation





__global__ void kernel (const float2* input_h0, const float* input_omega, float2* output_ht, unsigned int immutable_actualdim,unsigned int immutable_inwidth,unsigned int immutable_outwidth,unsigned int immutable_outheight,unsigned int immutable_dddressoffset,unsigned int immutable_addressoffset,float perframe_time) {

/* kernel pre-conditions */

__requires(immutable_actualdim == 512 /*actual_dim*/);

__requires(immutable_inwidth == 516 /*input_width*/);

__requires(immutable_outwidth == 512 /*output_width*/);

__requires(immutable_outheight == 512 /*output_height*/);

__requires(immutable_dddressoffset == 512*512 /*dtx_offset*/);

__requires(immutable_addressoffset == 512*512*2 /*dty_offset*/);





__requires(blockDim.x == 64);


__requires(blockDim.y == 64);




__requires(gridDim.x == 8);


__requires(gridDim.y == 8);




        int in_index = (blockIdx.y * blockDim.y + threadIdx.y) * immutable_inwidth + (blockIdx.x * blockDim.x + threadIdx.x);
        int in_mindex = (immutable_actualdim - (blockIdx.y * blockDim.y + threadIdx.y)) * immutable_inwidth + (immutable_actualdim - (blockIdx.x * blockDim.x + threadIdx.x));
        int out_index = (blockIdx.y * blockDim.y + threadIdx.y) * immutable_outwidth + (blockIdx.x * blockDim.x + threadIdx.x);

        // H(0) -> H(t)
        float2 h0_k  = input_h0[in_index];
        float2 h0_mk = input_h0[in_mindex];
        float sin_v, cos_v;

      //sin_v = sincos(input_omega[in_index] * perframe_time, &cos_v);
        sin_v = sin(input_omega[in_index] * perframe_time);
        cos_v = cos(input_omega[in_index] * perframe_time);

        float2 ht;
        ht.x = (h0_k.x + h0_mk.x) * cos_v - (h0_k.y + h0_mk.y) * sin_v;
        ht.y = (h0_k.x - h0_mk.x) * sin_v + (h0_k.y - h0_mk.y) * cos_v;

        // H(t) -> Dx(t), Dy(t)
        float kx = (blockIdx.x * blockDim.x + threadIdx.x) - immutable_actualdim * 0.5f;
        float ky = (blockIdx.y * blockDim.y + threadIdx.y) - immutable_actualdim * 0.5f;
        float sqr_k = kx * kx + ky * ky;
        float rsqr_k = 0;
        if (sqr_k > 1e-12f) {
            rsqr_k = 1 / sqrt(sqr_k);
        }
        kx *= rsqr_k;
        ky *= rsqr_k;

        float2 dt_x;

        dt_x.x = ht.y * kx;
        dt_x.y = -ht.x * kx;

        float2 dt_y;

        dt_y.x = ht.y * ky;
        dt_y.y = -ht.x * ky;

        if (((blockIdx.x * blockDim.x + threadIdx.x) < immutable_outwidth) && 
            ((blockIdx.y * blockDim.y + threadIdx.y) < immutable_outwidth))
        {
            output_ht[out_index] = ht;
            output_ht[out_index + immutable_dddressoffset] = dt_x;
            output_ht[out_index + immutable_addressoffset] = dt_y;		
#ifdef MUTATION
            output_ht[out_index+1] = output_ht[out_index+1];
               /* BUGINJECT: ADD_ACCESS, UP */
#endif
        }    

}
