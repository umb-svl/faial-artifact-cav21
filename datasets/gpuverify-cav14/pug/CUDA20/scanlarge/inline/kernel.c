#ifndef _MY_CUTIL_H_
#define _MY_CUTIL_H_

// variable modifiers
#define __shared__ volatile
#define __device__ volatile
#define __constant__ const

// function modifiers
#define __global__ static
#define __kernel__ inline
#define __host__ extern

typedef struct { char x; } char1;
typedef struct { unsigned char x; } uchar1;
typedef struct { short x; } short1;
typedef struct { unsigned short x; } ushort1;
typedef struct { int x; } int1;
typedef struct { unsigned int x; } uint1;
typedef struct { long x; } long1;
typedef struct { unsigned long x; } ulong1;
typedef struct { float x; } float1;
typedef struct { char x, y; } char2;
typedef struct { unsigned char x, y; } uchar2;
typedef struct { short x, y; } short2;
typedef struct { unsigned short x, y; } ushort2;
typedef struct { int x, y; } int2;
typedef struct { unsigned int x, y; } uint2;
typedef struct { long x, y; } long2;
typedef struct { unsigned long x, y; } ulong2;
typedef struct { float x, y; } float2;
typedef struct { char x, y, z; } char3;
typedef struct { unsigned char x, y, z; } uchar3;
typedef struct { short x, y, z; } short3;
typedef struct { unsigned short x, y, z; } ushort3;
typedef struct { int x, y, z; } int3;
typedef struct { unsigned int x, y, z; } uint3;
typedef struct { long x, y, z; } long3;
typedef struct { unsigned long x, y, z; } ulong3;
typedef struct { float x, y, z; } float3;
typedef struct { char x, y, z, w; } char4;
typedef struct { unsigned char x, y, z, w; } uchar4;
typedef struct { short x, y, z, w; } short4;
typedef struct { unsigned short x, y, z, w; } ushort4;
typedef struct { int x, y, z, w; } int4;
typedef struct { unsigned int x, y, z, w; } uint4;
typedef struct { long x, y, z, w; } long4;
typedef struct { unsigned long x, y, z, w; } ulong4;
typedef struct { float x, y, z, w; } float4;

typedef uint3 dim3;
typedef char CUTBoolean;
extern dim3 gridDim;
extern uint3 blockIdx;
extern dim3 blockDim;
extern uint3 threadIdx;
extern int cudaMemcpyHostToDevice;
extern int cudaMemcpyDeviceToHost;
extern void __syncthreads();

extern uint4 make_uint4(unsigned int x, unsigned int y, unsigned int z, unsigned int w);

#define __requires(x) assume(x)
extern void assert(int expression);
extern void assume(int expression);
extern int is_sv(int exp);

extern unsigned int threadIdx_x;
extern unsigned int threadIdx_y;
extern unsigned int threadIdx_z;

#define atomicAdd(x,y) x * y
#define bar __syncthreads
#define _mul24(x,y) x * y
#define __umul24(x,y) x * y

#endif

#define N 32

// Define this to more rigorously avoid bank conflicts, 
// even at the lower (root) levels of the tree
// Note that due to the higher addressing overhead, performance 
// is lower with ZERO_BANK_CONFLICTS enabled.  It is provided
// as an example.
//#define ZERO_BANK_CONFLICTS 

// 16 banks on G80
#define NUM_BANKS 16
#define LOG_NUM_BANKS 4

#ifdef ZERO_BANK_CONFLICTS
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS + (index) >> (2*LOG_NUM_BANKS))
#else
#define CONFLICT_FREE_OFFSET(index) ((index) >> LOG_NUM_BANKS)
#endif





__global__ void kernel (float* g_odata, float* g_idata, float* g_blockSums, int n,int blockIndex,int baseIndex,int storeSum,int isNP2) {




__requires(blockDim.x == 32);


__requires(blockDim.y == 1);




__requires(gridDim.x == 1);


__requires(gridDim.y == 1);




int ai, bi, mem_ai, mem_bi, bankOffsetA, bankOffsetB;
/*extern*/ __shared__ float s_data[N*2];

// ------------------------------------------------------------------------
// loadSharedChunkFromMem()
// ------------------------------------------------------------------------
baseIndex = (baseIndex == 0) ?  blockIdx.x * (blockDim.x << 1) : baseIndex;
int thid = threadIdx.x;
mem_ai = baseIndex + threadIdx.x;
mem_bi = mem_ai + blockDim.x;

ai = thid;
bi = thid + blockDim.x;

// compute spacing to avoid bank conflicts
bankOffsetA = CONFLICT_FREE_OFFSET(ai);
bankOffsetB = CONFLICT_FREE_OFFSET(bi);

// Cache the computational window in shared memory
// pad values beyond n with zeros

s_data[ai + bankOffsetA] = g_idata[mem_ai];

if (isNP2 != 0) // compile-time decision
{
  s_data[bi + bankOffsetB] = (bi < n) ? g_idata[mem_bi] : 0;
}
else
{
  s_data[bi + bankOffsetB] = g_idata[mem_bi];
}

// ------------------------------------------------------------------------
// prescanBlock()
// ------------------------------------------------------------------------

// ------------------------------------------------------------------------
// -- buildSum()
// ------------------------------------------------------------------------
unsigned int stride = 1;
// build the sum in place up the tree
for (int d = blockDim.x;
        d > 0; d >>= 1)
{
  __syncthreads();

  stride *= 2;

  if (thid < d)
  {
    int i_1  = stride * thid;
    int ai_1 = i_1 + stride/2 - 1;
    int bi_1 = ai_1 + stride/2;

    ai_1 += CONFLICT_FREE_OFFSET(ai_1);
    bi_1 += CONFLICT_FREE_OFFSET(bi_1);

#ifdef MUTATION
    s_data[0] += s_data[ai_1];
#else
    s_data[bi_1] += s_data[ai_1];
#endif
      /* BUGINJECT: MUTATE_OFFSET, UP, ZERO */
  }
}

// ------------------------------------------------------------------------
// -- clearLastElement()
// ------------------------------------------------------------------------
blockIndex = (blockIndex == 0) ? blockIdx.x : blockIndex;
if (threadIdx.x == 0)
{
  int index = (blockDim.x << 1) - 1;
  index += CONFLICT_FREE_OFFSET(index);

  if (storeSum != 0) // compile-time decision
  {
    // write this block's total sum to the corresponding index in the blockSums array
    g_blockSums[blockIndex] = s_data[index];
  }

  // zero the last element in the scan so it will propagate back to the front
  s_data[index] = 0;
}

// ------------------------------------------------------------------------
// -- scanRootToLeaves()
// ------------------------------------------------------------------------
for (int d = 1; d <= blockDim.x; d *= 2)
{
  stride >>= 1;

  __syncthreads();

  if (thid < d)
  {
    int i_2  =  2 * stride * thid;
    int ai_2 = i_2 + stride - 1;
    int bi_2 = ai_2 + stride;
    ai_2 += CONFLICT_FREE_OFFSET(ai_2);
    bi_2 += CONFLICT_FREE_OFFSET(bi_2);

    float t      = s_data[ai_2];
    s_data[ai_2] = s_data[bi_2];
    s_data[bi_2] += t; 
  }
}

// ------------------------------------------------------------------------
// storeSharedChunkToMem()
// ------------------------------------------------------------------------
__syncthreads();

// write results to global memory
g_odata[mem_ai] = s_data[ai + bankOffsetA]; 
if (isNP2 != 0) // compile-time decision
{
  if (bi < n)
    g_odata[mem_bi] = s_data[bi + bankOffsetB]; 
}
else
{
  g_odata[mem_bi] = s_data[bi + bankOffsetB]; 
}

}
