#/usr/bin/env python3

import sys
import argparse
import subprocess
import tomlkit
import csv
from toml import TomlEncoder
import copy

def analyze_loops(filename):
    cmd = ["faial", "--parse-gv-args", "-A", "--steps", "2", filename]
    max_nesting = 0
    sync_loops = 0
    unsync_loops = 0
    for line in subprocess.check_output(cmd, stderr=subprocess.DEVNULL).decode("utf-8").split("\n"):
        start = line.strip()
        if start.startswith("foreach*"):
            max_nesting = max(max_nesting, int(len(line.split("f", 1)[0])/4))
            sync_loops += 1
        elif start.startswith("foreach"):
            unsync_loops += 1
    return {"max_sync_nesting": max_nesting, "sync_loop_count": sync_loops, "unsync_loop_count": unsync_loops}

analyze_loops_keys = {"max_sync_nesting", "sync_loop_count", "unsync_loop_count"}

def contains_all(data, keys):
    return len(keys - set(data.keys())) == 0

def analyze_inst_kind(filename):
    cmd = ["faial", "--parse-gv-args", "-A", "--steps", "0", filename]
    loops = 0
    writes = 0
    reads = 0
    ifs = 0
    syncs = 0

    for line in subprocess.check_output(cmd, stderr=subprocess.DEVNULL).decode("utf-8").split("\n"):
        tag = line.strip()
        if tag.startswith("foreach ("):
            loops += 1
        elif tag.startswith("ro "):
            reads += 1
        elif tag.startswith("rw "):
            writes += 1
        elif tag.startswith("if ("):
            ifs += 1
        elif tag.startswith("sync;"):
            syncs += 1
    return {
        "loop_count": loops,
        "write_count": writes,
        "read_count": reads,
        "if_count": ifs,
        "sync_count": syncs,
    }

analyze_inst_kind_keys = {
        "loop_count",
        "write_count",
        "read_count",
        "if_count",
        "sync_count",
    }

def get_loop_kind(filename):
    cmd = ["faial", "--parse-gv-args", "-A", "--steps", "2", filename]
    max_nesting = 0
    for line in subprocess.check_output(cmd).decode("utf-8").split("\n"):
        if line.strip().startswith("foreach*"):
            max_nesting = max(max_nesting, int(len(line.split("f", 1)[0])/4))
    return max_nesting

def get_toml(filename):
    return filename[len("faial/"):-len("cu")] + "toml"

def update_stats(filename):
    toml_filename = get_toml(filename)
    with open(toml_filename) as fp:
        data = tomlkit.parse(fp.read())
    orig = dict(data.items())
    if not contains_all(data, analyze_loops_keys):
        try:
            data.update(analyze_loops(filename))
        except subprocess.CalledProcessError:
            print("WARNING: could not update sync/unsync loop info of file", filename)
    if not contains_all(data, analyze_inst_kind_keys):
        try:
            data.update(analyze_inst_kind(filename))
        except subprocess.CalledProcessError:
            print("WARNING: could not inst stats of file", filename)
    if "line_count" not in data:
        data["line_count"] = data["body"].count("\n")

    if dict(data.items()) != orig:
        print("updated", get_toml(filename))
        with open(get_toml(filename), "w") as fp:
            fp.write(tomlkit.dumps(data))

def update_from_csv(filename):
    with open(filename) as fp:
        for row in csv.DictReader(fp):
            update_stats(row["filename"])

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--csv",
        action='append',
        default=[],
        help="A csv with a 'filename' column."
    )
    parser.add_argument("-f", "--file",
        dest="filenames",
        action='append',
        default=[],
        help="Give a .cu file from the faial dataset.")
    args = parser.parse_args()
    for filename in args.csv:
        update_from_csv(filename)
    for filename in args.filenames:
        update_stats(filename)

main()