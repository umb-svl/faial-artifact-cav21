#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (int* x, int n) {



__requires(n > 0);

x[threadIdx.x + 1] = 0; // 0: x[1], 1: x[2], ...
for (int y = 0; y < n; y++) {
    x[threadIdx.x] = y; // 1: x[1], 2: x[2], ... (racy writes!)
    __syncthreads();
}

}
int main () {
    /* Declare scalar 'n' */
    int n;
    klee_make_symbolic(&n, sizeof(int), "n");
    
    /* Declare array 'x' */
    int *x;
    cudaMalloc((void**)&x, 1024 * sizeof(int));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        x,
        n
    );
    return 0;
}
