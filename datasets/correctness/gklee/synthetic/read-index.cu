#include <stdio.h>
#define __requires(x) klee_assume(x)







__global__ void kernel (int* x) {



x[threadIdx.x] = threadIdx.x;
int z = x[threadIdx.x];
x[z] = 0;

}
int main () {
    
    /* Declare array 'x' */
    int *x;
    cudaMalloc((void**)&x, 1024 * sizeof(int));
    dim3 grid_dim(1);
    dim3 block_dim(64);
    kernel<<< grid_dim, block_dim >>>(
        x
    );
    return 0;
}
