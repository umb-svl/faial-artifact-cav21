----- Data-race 1/1 invalid -----
The following race is impossible.  As both accesses are in the same sync loop,
the loop iter w in each access will be identical.  4:wr[tid + w_1 + 1] is
disjoint with 36:wr[tid + w_2 + 1] as w_1 == w_2.

last-iter-first-iter.cu: error: possible write-write race on x[49157]:

Write by thread 4 in thread block 0 (global id 4), last-iter-first-iter.cu:25:28:
  x[threadIdx.x + w + 1] = w; //     (first iter) 0: x[1 + 2n], ... (racy)

Write by thread 36 in thread block 0 (global id 36), last-iter-first-iter.cu:25:28:
  x[threadIdx.x + w + 1] = w; //     (first iter) 0: x[1 + 2n], ... (racy)

Bitwise values of parameters of 'kernel':
 n = 21829
