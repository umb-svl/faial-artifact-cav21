//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (int* x, int n) {



__requires(n > 2);
__requires(n <= 5592384);

for (int y = 1; y <= n; y++) {
    for (int z = 1; z <= y; z++) {
        __syncthreads();
        x[threadIdx.x + y + z] = z;
    }
}
for (int w = n * 2; w < n * 3; w++) {
    x[threadIdx.x + w] = w; // race fix: changed x[tid+2n+1] to x[tid+2n]
    __syncthreads();
}

}
