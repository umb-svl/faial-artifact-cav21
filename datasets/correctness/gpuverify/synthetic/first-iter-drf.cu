//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (int* x, int n) {



__requires(n > 0);

x[threadIdx.x + 1] = 0;
for (int y = 0; y < n; y++) {
    if (y > 0) { // this fixes racyness in first-iter
        x[threadIdx.x] = y;
    }
    __syncthreads();
}

}
