#!/usr/bin/env python3
import yaml
import toml
import argparse
import csv
from pathlib import Path
from tabulate import tabulate

def main():
    with open('config.yaml') as config_file:
        config = yaml.safe_load(config_file)
    kernels = map(Path, config['kernels'])
    tools = list(config['tools'].keys())
    default_tools = config['default_tools']
    parser = argparse.ArgumentParser(description="Generates table for RQ1.")
    parser.add_argument('--tools', nargs='*', choices=tools, default=[],
            help=f"Tools to display in table. Default: {default_tools}")
    parser.add_argument('--latex', default=False, action='store_true',
            help="Output table in LaTeX.")
    args = parser.parse_args()
    if not args.tools:
        args.tools = default_tools
    table(kernels, args.tools, args.latex)

def table(kernels, tools, latex=False):
    data = []
    for kernel in kernels:
        # add example, expected to data
        with open(kernel) as kernel_file:
            kernel_toml = toml.load(kernel_file)
        expected = kernel_toml.get('pass', 'unknown')
        if expected == True:
            expected = 'drf'
        elif expected == False:
            expected = 'racy'
        data.append([kernel.stem, expected])
    for tool in tools:
        fn_start = len(tool) + 1
        with open(f"timings-{tool}.csv") as fp:
            timings = list(csv.DictReader(fp))
        for row in range(len(timings)):
            kernel = Path(timings[row]['filename'][fn_start:]).stem
            result = timings[row]['data_races']
            assert data[row][0] == kernel # failure -> CSV rows don't match
            if latex:
                result = latex_results(data[row][1], result)
            data[row].append(result)
    format = 'latex_raw' if latex else 'simple'
    print(tabulate(data, headers=['example', 'expected'] + tools, tablefmt=format))

def latex_results(expected, observed):
    if 'error' in observed:
        return r'\UNS'
    if observed == 'timeout':
        return r'\TMO'
    if observed == 'no race alarms':
        observed = 'drf' # for consistency in the next comparison:
    if expected == observed:
        return r'\COR'
    else:
        return r'\INC'

if __name__ == '__main__':
    main()
