----- Data-race 1/1 valid -----
The following race is correctly reported.

Array:   x[7]
T1 mode: W
T2 mode: W

----------------
 Globals  Value 
----------------
 n        3 
----------------

---------------------
 Locals       T1  T2 
---------------------
 threadIdx.x  1   0 
---------------------

