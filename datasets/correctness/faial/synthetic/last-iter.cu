//pass
//--blockDim=[64] --gridDim=[1]

#include <cuda.h>






__global__ void kernel (int* x, int n) {



__requires(n > 0);

for (int y = 0; y < n; y++) {
    __syncthreads();
    x[threadIdx.x + 1] = y;      // 63: x[64]
}
x[threadIdx.x + blockDim.x] = 0; //  0: x[64] (racy write!)

}
