----- Data-race 1/1 invalid -----
This race report is impossible for the following reasons:
- Reported thread IDs {0, 3} do not race.  We expect an off-by-one thread race.
- We expect that y == z == n.  However, y == n == 7 but z == 6.


containBarrierFlag = 1
incrExpr = y++
------------- Already normalized 
rangeExpr = ((!1^y0 >= 1) && (!1^y0 <= n0)); substExpr = 
containBarrierFlag = 1
incrExpr = z++
------------- Already normalized 
rangeExpr = ((!2^z0 >= 1) && (!2^z0 <= !1^y0)); substExpr = 

FunctionCall: __syncthreads()

Binary OP: x[((threadIdx.x) +(y)) +(z)] = z
containBarrierFlag = 1
incrExpr = w++
------------- Already normalized 
rangeExpr = ((!3^w0 >= (n0 << 1)) && (!3^w0 <= ((n0 * 3) - 1))); substExpr = 

Binary OP: x[((threadIdx.x) +(w)) +(1)] = w

FunctionCall: __syncthreads()
satisfiable
(= n0 0b0000000110)
(= t1.x 0b0000000000)
(= !1^y0 0b0000000110)
(= !2^z0 0b0000000100)
(= t2.x 0b0000000011)
(= !3^w0 0b0000001100)
(= bdim.x 0b0001000000)
(= gdim.x 0b0000000001)
