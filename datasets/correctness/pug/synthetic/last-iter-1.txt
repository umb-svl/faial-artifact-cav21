----- Data-race 1/1 invalid -----
The reported race is mostly correct. The following values check out:

t1: 0:wr[tid + |T|]
t2: 63:wr[tid + 1]

As |T| = 64, both threads write to x[64], which is correct.  However, we expect
this race to occur on the last loop iteration (when y == n-1).  And
unfortunately, the reported race occurs well before then.  The values below of
y == 16 and n = 1008 do not reflect the last loop iteration.


containBarrierFlag = 1
incrExpr = y++
------------- Already normalized 
rangeExpr = ((!1^y0 >= 0) && (!1^y0 <= (n0 - 1))); substExpr = 

FunctionCall: __syncthreads()

Binary OP: x[(threadIdx.x) +(1)] = y

Binary OP: x[(threadIdx.x) +(blockDim.x)] = 0

Reaching the end of the kernel
satisfiable
(= t1.x 0b0000000000)
(= t2.x 0b0000111111)
(= bdim.x 0b0001000000)
(= n0 0b1111110000)
(= !1^y0 0b0000010000)
(= gdim.x 0b0000000001)
