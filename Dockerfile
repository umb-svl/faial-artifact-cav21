# Use the offical Ubuntu image as the parent.
FROM ubuntu:20.04

# Set up the working directory.
RUN useradd -m verify
WORKDIR /home/verify

# Install git and checkout the artifact repository.  Also install text editors.
USER root
RUN apt-get update -y \
  && apt-get install -y \
     git \
     vim \
     nano \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install Faial dependencies.
USER root
ENV PATH="$PATH:/opt/z3/bin"
ARG Z3_VERSION=4.8.8
ARG Z3_ARCH=x64-ubuntu-16.04
RUN cd /opt \
  && apt-get -y update \
  && apt-get install -y \
      libllvm10 \
      wget \
      unzip \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && wget https://github.com/Z3Prover/z3/releases/download/z3-${Z3_VERSION}/z3-${Z3_VERSION}-${Z3_ARCH}.zip \
  && unzip z3-${Z3_VERSION}-${Z3_ARCH}.zip \
  && mv z3-${Z3_VERSION}-${Z3_ARCH} z3 \
  && rm -f z3-${Z3_VERSION}-${Z3_ARCH}.zip \
  && z3 --version

# Install GKLEE & SESA dependencies.
USER root
RUN apt-get -y update \
  &&  apt-get install -y \
      python3 \
      libc6-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Note: PUG requires no additional dependencies.

# Install GPUVerify dependencies.
USER root
ARG DEBIAN_FRONTEND="noninteractive"
ENV TZ="America/New_York"
RUN apt-get -y update \
  &&  apt-get install -y \
      python \
      python-psutil \
      mono-runtime \
      libtinfo5 \
      libgomp1 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install SIMULEE dependencies.
USER root
RUN apt-get -y update \
  &&  apt-get install -y \
      python2.7 \
      python-numpy \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install ESBMC dependencies.
USER root
RUN apt-get -y update \
  &&  apt-get install -y \
      libz3-4 \
      libc6-dev-i386 \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Install verification framework dependencies.
USER root
RUN apt-get -y update \
  &&  apt-get install -y \
      time \
      python3 \
      python3-pip \
      texlive \
      texlive-latex-extra \
      texlive-fonts-recommended \
      cm-super \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
  && python3 -m pip install \
     toml==0.10.2 \
     Jinja2==2.11.2 \
     tabulate==0.8.7 \
     PyYAML==5.3.1 \
     numpy==1.18.2 \
     matplotlib==3.3.3 \
     pandas==1.1.4 \
     psutil==5.7.3 \
     scipy==1.5.4 \
     tomlkit==0.7.0

# Install Coq for mechanized proofs.
USER root
RUN apt-get -y update \
  &&  apt-get install -y \
      opam \
      libgmp3-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Clone the artifact repository to $FAIAL_HOME.  Change ownership to user
# verify, and make sure verify sources `env.sh` in `.bashrc`.  The script
# `env.sh` adds verification tools to the user's PATH.
USER root
ARG REPO_COMMIT=35abf4e8a5b0bdbe03ab72923d7e22c0f7eb3d82
ENV FAIAL_HOME="/artifact"
RUN git clone --recurse-submodules https://gitlab.com/umb-svl/faial-artifact-cav21.git $FAIAL_HOME \
  && chown -R verify:verify $FAIAL_HOME

USER verify
RUN cd $FAIAL_HOME \
  && git checkout $REPO_COMMIT -b working \
  && echo "cd \$FAIAL_HOME" >> /home/verify/.bashrc \
  && echo ". env.sh"        >> /home/verify/.bashrc

USER verify
ARG OPAMYES="yes"
RUN opam init --disable-sandboxing --disable-shell-hook \
  && echo 'eval $(opam config env)' >> ~/.bashrc \
  && eval $(opam config env) \
  && cd $FAIAL_HOME/faial-coq \
  && ./configure.sh \
  && opam clean -a -c -s --logs

# Run basic tests on verification tools.
USER verify
RUN cd $FAIAL_HOME \
  && . ./env.sh \
  && echo "Testing Faial..." \
  && faial tools/docker/hello-gv.cu \
  && echo "Testing GKLEE..." \
  && gklee-exec tools/docker/hello-gklee.cu \
  && echo "Testing SESA..." \
  && sesa-exec tools/docker/hello-gklee.cu \
  && echo "Testing PUG..." \
  && pug tools/docker/hello-pug.c \
  && echo "Testing GPUVerify..." \
  && gpuverify --gridDim 1 --blockDim 64 tools/docker/hello-gv.cu \
  && echo "Testing SIMULEE..." \
  && simulee-exec tools/docker/hello-gklee.cu \
  && echo "Testing ESBMC..." \
  && esbmc-gpu tools/docker/hello-esbmc.cu \
  && echo "Tests complete!"

# Run http.server to expose files inside container.  Also run bash.
USER verify
EXPOSE 8000
CMD nohup python3 -m http.server 8000 -d $FAIAL_HOME > /dev/null 2>&1 & bash
